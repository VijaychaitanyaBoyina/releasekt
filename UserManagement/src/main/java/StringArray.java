import java.util.*;

public class StringArray
{

	public boolean checkStringArray(String s1[],ArrayList<String> l){
        HashSet<String> hs1=new HashSet<String>(Arrays.asList (s1));
        hs1.remove ("");
        hs1.remove (null);
        HashSet<String> hs2=new HashSet<String>(l);
        hs2.remove ("");
        hs2.remove (null);
        if(hs1.size ()==hs2.size () && hs2.containsAll (hs1))
        return true;
        else
        return false;
    }
}