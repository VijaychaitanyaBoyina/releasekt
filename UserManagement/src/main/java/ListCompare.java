import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

public class ListCompare {
	
	private static List<String> sne = new ArrayList<String>();
	
	static{
		sne.add("");
		sne.add(null);
	}
	
	public boolean checkStringArray(String s1[],String s2[]){
        int len,count=0;
        HashSet<String> hs=new HashSet<String>(Arrays.asList (s1));
        hs.remove ("");
        hs.remove (null);
        len=hs.size ();
        ArrayList<String> al=new ArrayList<String>(Arrays.asList (s2));
        Iterator<String> i=hs.iterator ();
        while(i.hasNext ()){
            if(al.contains (i.next ()))
                count++;
        }
        
        if(count==len)
        return true;
        else
        return false;
    }
	
	public static boolean compareList(String[] strArr,List<String> strList){
		
		//Remove Null and Empty String in Array
		String[] retStrArr = removeNullEmptyArr(Arrays.asList(strArr));
		ArrayList retStrList = removeNullEmptyStr(strList);
		try{
			if(retStrArr.length!=retStrList.size()){
				return false;
			}else{
				for(int i=0;i<=retStrArr.length-1;i++){
					if(retStrList.indexOf(retStrArr[i])!=-1){
						retStrList.remove(retStrArr[i]);
					}else{
						return false;
					}
				}
				if(retStrList.size()==0){
					return true;
				}
			}
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public static String[] removeNullEmptyArr(List l){
		ArrayList al = new ArrayList(l);
		String[] strArr = null;
		try {
			al.removeAll(sne);
			strArr = new String[al.size()];
			strArr = (String[]) al.toArray(strArr);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return strArr;
	}
	
	public static ArrayList removeNullEmptyStr(List l){
		ArrayList al = null;
		try {
			al = (ArrayList) l;
			al.removeAll(sne);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return al;
	}
	public static void main(String[] args) {
		String[] str = {"","Sundar","Anil",null,"","Sundar"};
		List<String> strArrList = new ArrayList<String>();
		strArrList.add("");
		strArrList.add("Sundar");
		strArrList.add("Anil");
		strArrList.add(null);
		boolean comp = compareList(str, strArrList);
		System.out.println(comp);
	}
}
