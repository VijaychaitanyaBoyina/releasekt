import static org.junit.Assert.*;

import org.junit.Test;

import junit.framework.Assert;

/**
 * 
 */

/**
 * @author boyvij0
 *
 */
public class TestControlRoom {

	/**
	 * Test method for {@link ControlRoom#willTheBulbGlow(boolean, boolean)}.
	 */
	@Test
	public void testWillTheBulbGlow() {
		boolean switchOn = true;
		boolean powerOutage = false;
		
		ControlRoom cr=new ControlRoom();
		
		assertEquals(cr.willTheBulbGlow(switchOn, powerOutage),true);
		assertNotEquals(cr.willTheBulbGlow(switchOn, powerOutage),false);
		assertTrue(cr.willTheBulbGlow(switchOn, powerOutage));
		assertFalse(cr.willTheBulbGlow(switchOn, true));
	}

}
