import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class DataConsolidator {

	static List<String> dataList=new ArrayList<String>();
	static StringBuilder junkFiles=new StringBuilder();

	public DataConsolidator() {
		// TODO Auto-generated constructor stub
	}

	public static void readFromExcel(String colName, int colIndex, String fileName){

		try
		{
			int reqColVal = 0;
			boolean isVisited = false;

			FileInputStream file = new FileInputStream(new File(fileName));

			//Create Workbook instance holding reference to .xlsx file
			HSSFWorkbook workbook = new HSSFWorkbook(file);

			//Get first/desired sheet from the workbook
			HSSFSheet sheet = workbook.getSheetAt(0);

			//Iterate through each rows one by one
			Iterator<Row> rowIterator = sheet.iterator();
			while (rowIterator.hasNext()) 
			{
				Row row = rowIterator.next();
				//For each row, iterate through all the columns
				Iterator<Cell> cellIterator = row.cellIterator();

				while (cellIterator.hasNext()) 
				{
					Cell cell = cellIterator.next();
					//Check the cell type and format accordingly
					if(cell.getCellType() == Cell.CELL_TYPE_STRING){
						if(cell.getStringCellValue().contains(colName) ){
							reqColVal = cell.getColumnIndex();
							isVisited = true;
						}
					}
					if(cell.getColumnIndex() == reqColVal && isVisited == true)
					{
						switch (cell.getCellType()) 
						{
						case Cell.CELL_TYPE_NUMERIC:
							dataList.add(String.valueOf(cell.getNumericCellValue()));
							break;
						case Cell.CELL_TYPE_STRING:
							dataList.add(cell.getStringCellValue());
							break;
						}
					}
				}
			}
			if(isVisited == true){
				junkFiles.append(fileName+"\n");
				System.out.println(fileName);
			}
			file.close();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

	}

	public static void writeToExcel(String colName,List<String> inputList,  int colIndex){
		HSSFWorkbook workbook = new HSSFWorkbook(); 

		//Create a blank sheet
		HSSFSheet sheet = workbook.createSheet("Summary");
		sheet.createRow(0).createCell(colIndex - 1).setCellValue(colName);
		for (int rowIndex=1; rowIndex < inputList.size()  ; rowIndex++) {
			if( (inputList.get(rowIndex).contains(colName)) ){
			}else{
				Row row = sheet.createRow(rowIndex);
				Cell cell = row.createCell(colIndex - 1);
				cell.setCellValue(inputList.get(rowIndex));
			}
		}
		
		boolean isRowEmpty =false;
		for(int rowIndex=1; rowIndex <= sheet.getLastRowNum() ; rowIndex++){
			 if(sheet.getRow(rowIndex)==null){
		            isRowEmpty=true;
		            sheet.shiftRows(rowIndex + 1, sheet.getLastRowNum(), -1);
		            rowIndex--;
		        continue;
		        }
		        for(int colmIndex =0; colmIndex < sheet.getRow(rowIndex).getLastCellNum();colmIndex++){
		            if(sheet.getRow(rowIndex).getCell(colmIndex).toString().trim().equals("")){
		                isRowEmpty=true;
		            }else {
		                isRowEmpty=false;
		                break;
		            }
		        }
		        if(isRowEmpty==true){
		            sheet.shiftRows(rowIndex + 1, sheet.getLastRowNum(), -1);
		            rowIndex--;
		        }
		}

		try 
		{
			//Write the workbook in file system
			FileOutputStream out = new FileOutputStream(new File("P:\\Macro Output\\Consolodated Sheet3.xlsx"));
			workbook.write(out);
			out.close();

		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

	}
	
	public static List<String> getFileNamesFromDir(String dirPath){
		List<String> fileList=new ArrayList<String>();
		File folder = new File(dirPath);
		File[] listOfFiles = folder.listFiles();

		    for (int i = 0; i < listOfFiles.length; i++) {
		      if (listOfFiles[i].isFile() ) {
		    	  fileList.add(listOfFiles[i].getAbsolutePath());
		      } else if (listOfFiles[i].isDirectory()) {
		      }
		    }
		    return fileList;
	}

	public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException {
		
		List<String> filLst=getFileNamesFromDir("P:/SAIG/");
		for (String string : filLst) {
			readFromExcel("SAIG",1,string);
		}
//		PrintWriter writer = new PrintWriter("P:\\Macro Output\\Discardables.txt", "UTF-8");
//	    writer.println(junkFiles);
		//writeToExcel("SAIG",dataList, 1);
	}

}
